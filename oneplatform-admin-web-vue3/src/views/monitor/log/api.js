import { http } from '@/utils/http/axios';

const apiPrefix = '/actionlog';
export function GetList(query) {
  return http.request({
    url: apiPrefix + '/list',
    method: 'post',
    data: query,
  });
}


export function GetObj(id) {
  return http.request({
    url: apiPrefix + '/details?id=' + id,
    method: 'get',
  });
}

