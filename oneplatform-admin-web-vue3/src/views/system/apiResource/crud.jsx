import * as api from './api';
import { dict, compute } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const editRequest = async ({ form, row }) => {
    form.id = row.id;
    return await api.UpdateObj(form);
  };
  const delRequest = async ({ row }) => {
    return await api.DelObj(row.id);
  };

  const addRequest = async ({ form }) => {
    return await api.AddObj(form);
  };
  
  const httpMethodDict = dict({
    cloneable: false, // 关闭cloneable，任何情况下，都使用同一个dict
    data: [
      { value: 'GET', label: 'GET' },
      { value: 'POST', label: 'POST' },
    ],
  });

  const permissionLevelDict = dict({
    cloneable: false, // 关闭cloneable，任何情况下，都使用同一个dict
    data: [
      { value: 'Anonymous', label: '匿名访问' },
      { value: 'LoginRequired', label: '登录访问' },
      { value: 'PermissionRequired', label: '授权访问' },
    ],
  });
  
  const message = useMessage();
  return {
    crudOptions: {
      request: {
        pageRequest,
        addRequest,
        editRequest,
        delRequest,
      },
      search: {
        show: true,
      },
      rowHandle: {
        width: '150px',
        fixed: 'right',
      },
      toolbar: {
        show: true,
      },
      actionbar: {
        show: true,
        buttons: {
          add: {
            show: true,
          },
          syncApi: {
            text: '同步接口',
            show: true,
            click() {
              let moduleId = expose.getSearchFormData().moduleId;
              api.SyncModuleApis(moduleId);
              message.success('提交成功');
            },
          },
        },
      },
      columns: {
        id: {
          title: 'ID',
          key: 'id',
          type: 'number',
          column: {
            width: '70px',
          },
          form: {
            show: false,
          },
        },
        moduleId: {
          title: '所属模块',
          search: {
            show: true,
          },
          column: {
            width: '80px',
          },
          type: 'dict-select',
          dict: dict({
            url: '/perm/system/module/options',
          }),
          form: {
            rule: [{ required: true, message: '请选择一个选项' }],
          },
        },
        name: {
          title: '名称',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: '120px',
          },
          form: {
            rule: [{ required: true, message: '请输入接口名称', trigger: 'blur' }],
            component: {
              maxlength: 16, // 原生属性要写在这里
              props: {
                type: 'text',
                showWordLimit: true,
              },
            },
          }
        },
        method: {
          title: '方法',
          search: { show: false },
          column: {
            width: '50px',
          },
          dict: httpMethodDict,
          type: 'dict-select',
          form: {
            rule: [{ required: true, message: '请选择一个选项' }],
          },
        },
        uri: {
          title: '请求uri',
          key: 'uri',
          type: 'text',
          column: {
            width: '120px',
          },
          form: {
            rule: [{ required: true, message: '请输入uri', trigger: 'blur' }],
          }
        },
        permissionLevel: {
          title: '权限级别',
          search: { show: false },
          column: {
            width: '80px',
          },
          dict: permissionLevelDict,
          type: 'dict-select',
          form: {
            rule: [{ required: true, message: '请选择一个选项' }],
          },
        },
        openapi: {
          title: 'openAPI',
          search: { show: false },
          column: {
            width: '60px',
          },
          type: 'dict-switch',
          dict: dict({
            data: [
              { value: true, label: '是' },
              { value: false, label: '否' },
            ],
          }),
        },
        enabled: {
          title: '状态',
          column:{
              width: '60px',
              cellRender(scope){
                if(scope.value)
                  return <n-tag type="success"> 启用 </n-tag>;
                else 
                  return <n-tag type="warning"> 禁用 </n-tag>;
              }
          },
          form: {
            show: false,
          },
        },
        updatedAt: {
          title: '最后更新时间',
          key: 'updatedAt',
          column: {
            width: 120,
          },
          form: {
            show: false,
          },
        },
      },
    },
  };
}
