import * as api from '../api';
import { dict } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';

export default function ({ expose, accountSelectorRef}) {
  const delRequest = async ({ row }) => {
    let param = {};
    param.roleId = row.firstId;
    param.userIds = [row.secondId];
    return await api.RemoveUsers(param);
  };
  
  const message = useMessage();

  return {
    crudOptions: {
      pagination: {
        showSizeChanger: false, // antdv
        showQuickJumper: false, // antdv
      },
      request: {
        pageRequest: api.GetUserList,
        delRequest,
      },
      search: {
        show: false,
      },
      toolbar: {
        show: false,
      },
      actionbar: {
        show: true,
        buttons: {
          add: {
            show: false,
          },
          syncApi: {
            text: '分配用户',
            show: true,
            click() {
              if(!expose.getSearchFormData().id){
                message.error('请先选择角色');
                return;
              }
              accountSelectorRef.value.showModal();
            },
          },
        },
      },
      rowHandle: {
        width: '100px',
        buttons: {
          view: { show: false },
          edit: { show: false },
          remove: { show: true },
        },
      },
      table: {},
      columns: {
        secondId: {
          title: '用户Id',
          column: {
            width: 120,
            align: 'center',
            sortable: true,
          },
        },
        secondName: {
          title: '用户名',
          type: 'text',
          column: {
            sortable: true,
          },
        },
      },
    },
  };
}
