import * as api from './api';
import { dict, compute } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const editRequest = async ({ form, row }) => {
    form.id = row.id;
    return await api.UpdateObj(form);
  };
  const delRequest = async ({ row }) => {
    return await api.DelObj(row.id);
  };

  const addRequest = async ({ form }) => {
    return await api.AddObj(form);
  };
  const message = useMessage();
  return {
    crudOptions: {
      request: {
        pageRequest,
        addRequest,
        editRequest,
        delRequest,
      },
      search: {
        show: false,
      },
      rowHandle: {
        width: '230px',
      },
      toolbar: {
        show: false,
      },
      columns: {
        id: {
          title: 'ID',
          key: 'id',
          type: 'number',
          column: {
            width: 50,
          },
          form: {
            show: false,
          },
        },
        name: {
          title: '名称',
          key: 'name',
          type: 'text',
          column: {
            width: 100,
          },
          form: {
            helper: '添加和编辑时必填，编辑时额外需要校验长度',
            rule: [{ required: true, message: '请输入服务名称', trigger: 'blur' }],
            component: {
              maxlength: 16, // 原生属性要写在这里
              props: {
                type: 'text',
                showWordLimit: true,
              },
            },
          }
        },
        serviceId: {
          title: '服务标识',
          key: 'serviceId',
          type: 'text',
          column: {
            width: 100,
          },
          form: {
            helper: '对应注册中心注册的服务名',
            rule: [{ required: true, message: '请输入服务标识', trigger: 'blur' }],
          }
        },
        routeName: {
          title: '路由',
          key: 'routeName',
          type: 'text',
          column: {
            width: 100,
          },
          form: {
            helper: '网关转发路径',
            rule: [{ required: true, message: '请输入服务路由', trigger: 'blur' }],
          }
        },
        proxyUrl: {
          title: '代理地址',
          key: 'proxyUrl',
          type: 'text',
          column: {
            width: 100,
          },
          form: {
            helper: '不填则使用lb://服务标识',
            show: true,
          },
        },
        enabled: {
          title: '启用状态',
          column:{
              width: 100,
              cellRender(scope){
                if(scope.value)
                  return <n-tag type="success"> 启用 </n-tag>;
                else 
                  return <n-tag type="warning"> 禁用 </n-tag>;
              }
          },
          form: {
            show: false,
          },
        },
        updatedAt: {
          title: '最后更新时间',
          key: 'updatedAt',
          column: {
            width: 120,
          },
          form: {
            show: false,
          },
        },
      },
    },
  };
}
