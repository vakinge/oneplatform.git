import * as api from './api';
import { dict, compute } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const editRequest = async ({ form, row }) => {
    form.id = row.id;
    return await api.UpdateObj(form);
  };
  const delRequest = async ({ row }) => {
    return await api.DelObj(row.id);
  };

  const addRequest = async ({ form }) => {
    return await api.AddObj(form);
  };
  

  const timeExprDict = dict({
    cloneable: false, // 关闭cloneable，任何情况下，都使用同一个dict
    data: [
      { value: null, label: '无' },
      { value: '{yyyy}{MM}{dd}', label: '{yyyy}{MM}{dd}' },
      { value: '{yyyy}{MM}', label: '{yyyy}{MM}' },
      { value: '{yyyy}', label: '{yyyy}' },
    ],
  });
  
  const message = useMessage();
  return {
    crudOptions: {
      request: {
        pageRequest,
        addRequest,
        editRequest,
        delRequest,
      },
      search: {
        show: true,
      },
      rowHandle: {
        width: '150px',
        fixed: 'right',
      },
      toolbar: {
        show: true,
      },
      actionbar: {
        show: true,
        buttons: {
          add: {
            show: true,
          },
        },
      },
      columns: {
        name: {
          title: '名称',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: '120px',
          },
          form: {
            rule: [{ required: true, message: '请输入规则名称', trigger: 'blur' }],
            component: {
              maxlength: 16, // 原生属性要写在这里
              props: {
                type: 'text',
                showWordLimit: true,
              },
            },
          }
        },
        code: {
          title: '标识',
          key: 'code',
          type: 'text',
          search: { show: true },
          column: {
            width: '120px',
          },
          form: {
            rule: [{ required: true, message: '请输入规则标识', trigger: 'blur' }],
            component: {
              maxlength: 32, // 原生属性要写在这里
              props: {
                type: 'text',
                showWordLimit: true,
              },
            },
          }
        },
        prefix: {
          title: '前缀',
          key: 'prefix',
          type: 'text',
          column: {
            width: '80px',
          },
        },
        timeExpr: {
          title: '时间规则',
          key: 'timeExpr',
          search: { show: false },
          column: {
            width: '100px',
          },
          dict: timeExprDict,
          type: 'dict-select',
        },
        seqLength: {
          title: '自增序列长度',
          key: 'seqLength',
          type: 'number',
          column: {
            width: '100px',
          },
        },
        firstSequence: {
          title: '初始序列',
          key: 'firstSequence',
          type: 'number',
          column: {
            width: '100px',
          },
        },
        lastSequence: {
          title: '当前序列',
          key: 'lastSequence',
          type: 'number',
          column: {
            width: '100px',
          },
          form: {
            show: false,
          },
        },
        enabled: {
          title: '状态',
          column:{
              width: '60px',
              cellRender(scope){
                if(scope.value)
                  return <n-tag type="success"> 启用 </n-tag>;
                else 
                  return <n-tag type="warning"> 禁用 </n-tag>;
              }
          },
          form: {
            show: false,
          },
        },
        updatedAt: {
          title: '最后更新时间',
          key: 'updatedAt',
          column: {
            width: 120,
          },
          form: {
            show: false,
          },
        },
      },
    },
  };
}
