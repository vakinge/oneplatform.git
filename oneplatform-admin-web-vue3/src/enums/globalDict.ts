import { dict } from '@fast-crud/fast-crud';

export const boolDict = dict({
    cloneable: false, 
    data: [
      { value: 'true', label: '是' },
      { value: 'false', label: '否' },
    ],
  });

export const orgTypeDict = dict({
    cloneable: false, 
    data: [
      { value: 'dept', label: '部门' },
      { value: 'bu', label: '事业部' },
      { value: 'corp', label: '总公司' },
      { value: 'branch', label: '分公司' },
    ],
  });
  
export const genderDict = dict({
    cloneable: false, 
    data: [
      { value: 'male', label: '男' },
      { value: 'female', label: '女' },
    ],
  });
  
