/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.support.oauth2;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.mendmix.common.model.AuthUser;
import com.mendmix.security.connect.oauth.OauthNextHandler;
import com.mendmix.security.connect.oauth.OauthNextResult;
import com.oneplatform.support.auth.LoginUserInfo;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Nov 13, 2022
 */
@Component
public class PortalOauthNextHandler implements OauthNextHandler<LoginUserInfo> {

	@Override
	public  OauthNextResult<LoginUserInfo> handle(String type, Map<String, Object> openUserInfo) {
		String openId = null;
		if("osc".equals(type)) {
			openId = openUserInfo.get("id").toString();
		}
		//TODO 查找本地用户
		LoginUserInfo loginUserInfo = new LoginUserInfo();
		loginUserInfo.setId("1000");
		loginUserInfo.setName("oscUser");
		//如果不指定跳转页面，则默认跳转到发起请求的页面
		String redirectUrl = null;
		return new OauthNextResult<>(loginUserInfo, redirectUrl);
	}

}
