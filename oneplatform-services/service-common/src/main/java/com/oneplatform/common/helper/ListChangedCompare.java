/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.common.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mendmix.mybatis.core.BaseEntity;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Jun 23, 2023
 */
public class ListChangedCompare<E extends BaseEntity> {

	private List<E> oldList;
	private final List<E> addList = new ArrayList<>();
	private final List<E> updateList = new ArrayList<>();
	private final List<E> removeList = new ArrayList<>();
	
	public ListChangedCompare(List<E> oldList) {
		super();
		this.oldList = oldList;
	}

	public ListChangedCompare<E> compareTo(List<E> newList){
		final boolean hasNewList = newList != null && !newList.isEmpty();
		if(hasNewList) {
			addList.addAll(newList);
			addList.removeAll(oldList);
		}
		removeList.addAll(oldList);
		if(hasNewList)removeList.removeAll(newList);
		if(hasNewList)updateList.addAll(newList);
		updateList.retainAll(oldList);
		
		if(!updateList.isEmpty()) {
			Iterator<E> iterator = updateList.iterator();
			while (iterator.hasNext()) {
				E updateEntity = iterator.next();
				for (E entity : oldList) {
					if (!entity.equals(updateEntity)){
						continue;
					}
				}
			}
		}
		return this;
	}

	public List<E> getAddList() {
		return addList;
	}

	public List<E> getUpdateList() {
		return updateList;
	}

	public List<E> getRemoveList() {
		return removeList;
	}
	
	
}
