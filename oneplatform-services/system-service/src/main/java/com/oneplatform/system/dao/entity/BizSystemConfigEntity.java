package com.oneplatform.system.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.system.dao.StandardBaseEntity;

@Table(name = "system_config")
public class BizSystemConfigEntity extends StandardBaseEntity {

	private static final long serialVersionUID = 1L;
	
	private String name;
    private String code;
    private String content;
    
    @Column(name = "is_default")
    private Boolean isDefault = Boolean.FALSE;
    
    @Column(name = "is_display")
    private Boolean isDisplay = Boolean.TRUE;
    
	@Column(name = "system_id",updatable = false)
    private Integer systemId;

	@Column(name = "tenant_id", updatable = false)
	private String tenantId;

	@Column(name = "client_type", updatable = false)
	private String clientType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	

}
