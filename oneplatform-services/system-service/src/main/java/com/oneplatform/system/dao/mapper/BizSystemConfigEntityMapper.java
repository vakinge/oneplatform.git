package com.oneplatform.system.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.BizSystemConfigEntity;


public interface BizSystemConfigEntityMapper extends BaseMapper<BizSystemConfigEntity,Integer> {
	
}
