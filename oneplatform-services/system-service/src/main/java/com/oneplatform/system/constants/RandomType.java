/**
 * Confidential and Proprietary Copyright 2019 By 卓越里程教育科技有限公司 All Rights Reserved
 */
package com.oneplatform.system.constants;

/**
 * 
 * <br>
 * Class Name   : RandomType
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2020年6月12日
 */
public enum RandomType {

	CHAR,NUMBER,ANY;
	
	public static boolean numbers(String value){
		return RandomType.NUMBER.name().equalsIgnoreCase(value) || RandomType.ANY.name().equalsIgnoreCase(value);
	}
	
	public static boolean chars(String value){
		return RandomType.CHAR.name().equalsIgnoreCase(value) || RandomType.ANY.name().equalsIgnoreCase(value);
	}
}
