package com.oneplatform.system.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.system.dao.StandardBaseEntity;


@Table(name = "api_resource")
public class ApiResourceEntity extends StandardBaseEntity {

	@Column(name = "system_id",updatable = false)
    private Integer systemId;
    
    @Column(name = "module_id",updatable = false)
    private Integer moduleId;

    /**
     * 资源名称
     */
    private String name;

    /**
     * uri
     */
    private String uri;

    @Column(name = "method")
    private String method;

    /**
     * 授权类型
     */
    @Column(name = "permission_level")
    private String permissionLevel;

    private Boolean openapi;

    
	public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	/**
     * 获取资源名称
     *
     * @return name - 资源名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置资源名称
     *
     * @param name 资源名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取uri
     *
     * @return uri - uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * 设置uri
     *
     * @param uri uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }


	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(String permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	public Boolean getOpenapi() {
		return openapi;
	}

	public void setOpenapi(Boolean openapi) {
		this.openapi = openapi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((moduleId == null) ? 0 : moduleId.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApiResourceEntity other = (ApiResourceEntity) obj;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		if (moduleId == null) {
			if (other.moduleId != null)
				return false;
		} else if (!moduleId.equals(other.moduleId))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

    

}