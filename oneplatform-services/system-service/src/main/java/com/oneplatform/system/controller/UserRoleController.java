package com.oneplatform.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.CurrentRuntimeContext;
import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.NameValuePair;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.model.SelectOption;
import com.mendmix.common.model.TreeModel;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.system.constants.FunctionResourceType;
import com.oneplatform.system.constants.GrantRelationType;
import com.oneplatform.system.constants.SubRelationType;
import com.oneplatform.system.dao.entity.FunctionResourceEntity;
import com.oneplatform.system.dao.entity.UserRoleEntity;
import com.oneplatform.system.dao.mapper.FunctionResourceEntityMapper;
import com.oneplatform.system.dto.ObjectRelation;
import com.oneplatform.system.dto.ResourceTreeModel;
import com.oneplatform.system.dto.UserRole;
import com.oneplatform.system.dto.param.FunctionResourceQueryParam;
import com.oneplatform.system.dto.param.GrantPermItem;
import com.oneplatform.system.dto.param.GrantRelationParam;
import com.oneplatform.system.dto.param.GrantUserRolePermParam;
import com.oneplatform.system.dto.param.RoleSetUserParam;
import com.oneplatform.system.dto.param.UserRoleParam;
import com.oneplatform.system.dto.param.UserRoleQueryParam;
import com.oneplatform.system.service.FunctionResourceService;
import com.oneplatform.system.service.InternalRelationService;
import com.oneplatform.system.service.UserPermissionService;
import com.oneplatform.system.service.UserRoleService;


@RestController
@RequestMapping("/role")
public class UserRoleController {

	@Autowired
	private UserRoleService userRoleService;
	@Autowired 
    private UserPermissionService userPermissionService;
	@Autowired
	private FunctionResourceService functionResourceService;
	@Autowired
	private InternalRelationService internalRelationService;
	@Autowired
    private FunctionResourceEntityMapper functionResourceMapper;

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping("add")
	public IdParam<Integer> add(@RequestBody UserRoleParam param) {
		String tenantId = CurrentRuntimeContext.getTenantId(false);
		return userRoleService.addUserRole(tenantId,param);
	}

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping("delete")
	public void delete(@RequestBody IdParam<Integer> param) {
		userRoleService.deleteUserRole(param.getId());
	}

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping("update")
	public void update(@RequestBody UserRoleParam param) {
		userRoleService.updateUserRole(param);
	}

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping("toggle")
	public void toggleUserGroup(@RequestBody IdParam<Integer> param) {
		userRoleService.toggleUserRole(param.getId());
	}

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@GetMapping("{id}")
	public UserRole get(@PathVariable("id") Integer id) {
		return userRoleService.getUserRole(id);
	}

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping("list")
	public Page<UserRole> pageQry(@RequestBody PageQueryRequest<UserRoleQueryParam> queryParam) {
		if(queryParam.getExample() == null)queryParam.setExample(new UserRoleQueryParam());
		PageParams pageParams = new PageParams(queryParam.getPageNo(), queryParam.getPageSize());
		return userRoleService.pageQryUserRole(pageParams,queryParam.getExample());
	}

	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@GetMapping("options")
	public List<SelectOption> options(@RequestParam(name="type") String type) {
		UserRoleQueryParam param = new UserRoleQueryParam();
		param.setRoleType(type);
		param.setEnabled(true);
		return userRoleService.listByQueryParam(param).stream().map( o -> {
			return new SelectOption(o.getId().toString(), o.getName());
		}).collect(Collectors.toList());
	}


	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@PostMapping(value = "/updateUsers")
	@ResponseBody
	public void updateRoleUsers(@RequestBody @Validated RoleSetUserParam param) {
		UserRoleEntity roleEntity = userRoleService.getUserRoleEntity(param.getRoleId());
		if(roleEntity == null || !roleEntity.getEnabled()) {
			throw new MendmixBaseException("角色不存在或已禁用");
		}
		String roleId = param.getRoleId().toString();
		List<String> userIds = param.getUserIds();
		internalRelationService.updateParentSubRelations(SubRelationType.userToRole, roleId, userIds);
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@PostMapping(value = "/removeUsers")
	@ResponseBody
	public void removeRoleUsers(@RequestBody @Validated RoleSetUserParam param) {
		String roleId = param.getRoleId().toString();
		List<String> userIds = param.getUserIds();
		
		for (String userId : userIds) {
			internalRelationService.deleteUserRoleRelations(roleId, userId);
		}
	}
	
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@PostMapping(value = "/grant")
	@ResponseBody
	public void grantPermissions(@RequestBody GrantUserRolePermParam param) {

		Map<String, List<String>> typeGroupMap = new HashMap<>(3);
		
		List<String> tmpList;
		for (GrantPermItem item : param.getPermItems()) {
			tmpList = typeGroupMap.get(item.getType());
			if (tmpList == null) {
				tmpList = new ArrayList<>();
				typeGroupMap.put(item.getType(), tmpList);
			}
			if(!tmpList.contains(item.getId().toString())) {				
				tmpList.add(item.getId().toString());
			}
		}
		
		//查询按钮归属的菜单
		if(typeGroupMap.containsKey(FunctionResourceType.button.name())){
			List<Integer> buttonIds = typeGroupMap.get(FunctionResourceType.button.name()).stream().map(o -> {
				return Integer.parseInt(o);
		    }).collect(Collectors.toList());
			List<FunctionResourceEntity> buttons = functionResourceMapper.selectByPrimaryKeys(buttonIds);
			for (FunctionResourceEntity button : buttons) {
				tmpList = typeGroupMap.get(FunctionResourceType.button.name());
				if (tmpList == null) {
					tmpList = new ArrayList<>();
					typeGroupMap.put(FunctionResourceType.menu.name(), tmpList);
				}
				if(!tmpList.contains(button.getParentId().toString())){					
					tmpList.add(button.getParentId().toString());
				}
			}
		}

		final List<GrantRelationParam> paramList = new ArrayList<>();
		typeGroupMap.forEach((k, v) -> {
			GrantRelationParam relationParam = new GrantRelationParam();
			relationParam.setRelationType(GrantRelationType.funcToRole);
			relationParam.setTargetId(param.getRoleId().toString());
			relationParam.setSourceIdList(v);
			paramList.add(relationParam);
		});
		userRoleService.updateGrantedPermissions(paramList);

	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = false)
	@PostMapping("users")
	public Page<ObjectRelation> pageQueryUsers(@RequestBody PageQueryRequest<IdParam<Integer>> param){
		if(param.getExample() == null || param.getExample().getId() == null) {
			return Page.blankPage(param.getPageNo(), param.getPageSize());
		}
		PageParams pageParam = new PageParams(param.getPageNo(), param.getPageSize());
		String pid = param.getExample().getId().toString();
		Page<ObjectRelation> page = internalRelationService.pageSubRelationChildren(pageParam, SubRelationType.userToRole, pid);
		return page;
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = false)
    @GetMapping(value = "buttons")
    @ResponseBody
    public List<NameValuePair> findRoleGrantedButtons(@RequestParam("roleId")Integer roleId) {
		String clientType = CurrentRuntimeContext.getClientType();
		List<FunctionResourceEntity> resources = userPermissionService.findRoleGrantedResources(FunctionResourceType.button, clientType, roleId);
		return resources.stream().map(o -> {
			return new NameValuePair(o.getName(), o.getCode());
		}).collect(Collectors.toList());
    }
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = false)
    @GetMapping(value = "resourceTree")
    @ResponseBody
	public List<TreeModel> getRoleFunctionResourceTree(@RequestParam("roleId") Integer roleId
            , @RequestParam(value = "includeButton", required = false, defaultValue = "true") boolean includeButton) {

		UserRole role = userRoleService.getUserRole(roleId);
		if(role == null)throw new MendmixBaseException("角色不存在");

        FunctionResourceQueryParam queryParam = new FunctionResourceQueryParam();
        if (!includeButton) queryParam.setType(FunctionResourceType.menu.name());
        queryParam.setEnabled(true);
        List<FunctionResourceEntity> entities = functionResourceService.findByQueryParam(queryParam);
        //当前系统所有功能资源
        List<ResourceTreeModel> allResorces = BeanUtils.copy(entities, ResourceTreeModel.class);

        //当前角色的功能资源
        List<FunctionResourceEntity> roleRresorces;
        if (includeButton) {
        	roleRresorces = userPermissionService.findRoleGrantedResources(null, null, roleId);
        }else {
        	roleRresorces = userPermissionService.findRoleGrantedResources(FunctionResourceType.menu, null, roleId);
        }
        if (!roleRresorces.isEmpty()) {
            List<String> roleRresorceIds = roleRresorces.stream().map(o -> o.getId().toString()).collect(Collectors.toList());
            //设置选中状态
            for (ResourceTreeModel resource : allResorces) {
                setRoleResourceChecked(resource, roleRresorceIds, false);
            }
        }

        return TreeModel.build(allResorces).getChildren();
    }
	
	private void setRoleResourceChecked(ResourceTreeModel resource, List<String> roleRresorceIds, boolean onlyLeaf) {
        if (roleRresorceIds.contains(resource.getId())) {
            if (!onlyLeaf || (onlyLeaf && resource.getChildren() == null)) {
                resource.setChecked(true);
            }
        }
        if (resource.getChildren() != null) {
            List<TreeModel> children = resource.getChildren();
            for (TreeModel child : children) {
                setRoleResourceChecked((ResourceTreeModel) child, roleRresorceIds, onlyLeaf);
            }
        }
    }
}
