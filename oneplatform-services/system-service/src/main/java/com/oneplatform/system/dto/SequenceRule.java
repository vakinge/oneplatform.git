package com.oneplatform.system.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * <br>
 * Class Name : Regulation
 */
public class SequenceRule extends SequenceRuleParam {

	@ApiModelProperty("最后自增序列值")
	private Integer lastSequence;

	@ApiModelProperty("激活状态")
	private Boolean enabled;

	private Date createdAt;
	private String createdBy;

	private Date updatedAt;
	private String updatedBy;

	/**
	 * @return the lastSequence
	 */
	public Integer getLastSequence() {
		return lastSequence;
	}

	/**
	 * @param lastSequence
	 *            the lastSequence to set
	 */
	public void setLastSequence(Integer lastSequence) {
		this.lastSequence = lastSequence;
	}

	/**
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}