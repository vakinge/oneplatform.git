package com.oneplatform.system.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mendmix.common.model.TreeModel;

@JsonInclude(Include.NON_NULL)
public class ResourceTreeModel extends TreeModel {

    private String type;
    private String code;
    private String clientType;
	private Boolean isDisplay;
	private Boolean isDefault;
	private Boolean isOpenAccess;
	
	private Map<String, String> specAttrs;
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public Boolean getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public Boolean getIsOpenAccess() {
		return isOpenAccess;
	}
	public void setIsOpenAccess(Boolean isOpenAccess) {
		this.isOpenAccess = isOpenAccess;
	}
	public Map<String, String> getSpecAttrs() {
		return specAttrs;
	}
	public void setSpecAttrs(Map<String, String> specAttrs) {
		this.specAttrs = specAttrs;
	}
	
	
}
