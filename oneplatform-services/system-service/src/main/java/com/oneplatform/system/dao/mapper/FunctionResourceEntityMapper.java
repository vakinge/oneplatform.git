package com.oneplatform.system.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.FunctionResourceEntity;
import com.oneplatform.system.dto.param.FunctionResourceQueryParam;

public interface FunctionResourceEntityMapper extends BaseMapper<FunctionResourceEntity, Integer> {
	
    /**
     * 根据查询参数获取菜单资源列表(模糊查询）
     * @param queryParam
     * @return
     */
    List<FunctionResourceEntity> findByQueryParam(FunctionResourceQueryParam queryParam);

    List<FunctionResourceEntity> findParentMenus(@Param("clientType") String clientType);
    
    List<FunctionResourceEntity> findRoleGrantedResources(@Param("roleIds") List<String> roleIds,@Param("clientType") String clientType,@Param("resourceType") String resourceType);

    @Select("SELECT * FROM function_resource WHERE client_type = #{clientType} AND type = #{resourceType} AND is_open_access = 1 AND enabled = 1")
    @ResultMap("BaseResultMap")
    List<FunctionResourceEntity> findOpenAcessResources(@Param("clientType") String clientType,@Param("resourceType") String resourceType);
}