DROP TABLE IF EXISTS `biz_system`;
CREATE TABLE `biz_system` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `category` varchar(32) DEFAULT NULL,
  `api_baseurl` varchar(100) DEFAULT NULL,
  `master_uid` varchar(32) DEFAULT NULL,
  `master_uname` varchar(32) DEFAULT NULL,
  `master_phone` varchar(32) DEFAULT NULL,
  `secret` varchar(64) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT 1 COMMENT '是否内部系统',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  `enabled` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='业务系统';


DROP TABLE IF EXISTS `system_module`;
CREATE TABLE `system_module` (
  `id` int(10)  NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `service_id` varchar(64) DEFAULT NULL,
  `route_name` varchar(32) DEFAULT NULL,
  `proxy_url` varchar(100) DEFAULT NULL,
  `anonymous_uris` varchar(500) DEFAULT NULL,
  `route_rules` varchar(500) DEFAULT NULL,
  `system_id` int(10) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  `enabled` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='系统服务模块';

DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` varchar(64) NOT NULL,
  `content` varchar(500) NOT NULL,
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否系统默认',
  `is_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `client_type` varchar(32) NOT NULL COMMENT '用户端类型',
  `system_id` int(10) DEFAULT NULL,
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '可用状态：0=禁用; 1=启用',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='系统配置';

DROP TABLE IF EXISTS `function_resource`;
CREATE TABLE `function_resource` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL COMMENT '父ID',
  `type` enum('catalog','menu','button','page','widget') NOT NULL COMMENT '类型',
  `name` varchar(50) NOT NULL COMMENT '资源名称',
  `code` varchar(64) NOT NULL COMMENT '编码',
  `spec_attrs` varchar(1000) NOT NULL COMMENT '配置',
  `client_type` enum('pc','mobile','pad') DEFAULT 'pc' COMMENT '用户端类型',
  `is_open_access` tinyint(1) DEFAULT '0' COMMENT '是否开放访问',
  `tags` varchar(64) NOT NULL COMMENT '标签',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否系统默认',
  `is_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `sort` int(2) DEFAULT '99' COMMENT '排序',
  `system_id` int(10) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '可用状态(0:不可用;1:可用)',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='功能资源';

DROP TABLE IF EXISTS `api_resource`;
CREATE TABLE `api_resource` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module_id` int(10) NOT NULL COMMENT '关联系统模块',
  `name` varchar(50) DEFAULT NULL COMMENT '资源名称',
  `method` enum('GET','POST') DEFAULT 'GET',
  `uri` varchar(200) DEFAULT NULL COMMENT 'uri',
  `permission_level` enum('Anonymous','LoginRequired','PermissionRequired') DEFAULT NULL COMMENT '权限级别',
  `openapi` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否对外发布openAPI',
  `system_id` int(10) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '可用状态(0:不可用;1:可用)',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='接口资源';


DROP TABLE IF EXISTS `grant_relations`;
CREATE TABLE `grant_relations` (
  `source_id` varchar(64) NOT NULL COMMENT '资源ID',
  `target_id` varchar(64) NOT NULL COMMENT '目标ID',
  `relation_type` enum('apiToRole','funcToRole') NOT NULL COMMENT '类型',
  `system_id` int(10) NOT NULL,
  PRIMARY KEY (`source_id`,`target_id`,`relation_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授权关系';



DROP TABLE IF EXISTS `subordinate_relations`;
CREATE TABLE `subordinate_relations` (
  `parent_id` varchar(64) NOT NULL COMMENT '父级ID',
  `child_id` varchar(64) NOT NULL COMMENT '子级ID',
  `relation_type` enum('userToRole') NOT NULL COMMENT '关系类型',
  `system_id` int(10) NOT NULL,
  PRIMARY KEY (`parent_id`,`child_id`,`relation_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='从属关系';

DROP TABLE IF EXISTS `binding_relations`;
CREATE TABLE `binding_relations` (
  `source_id` varchar(64) NOT NULL COMMENT '源ID',
  `target_id` varchar(64) NOT NULL COMMENT '目标ID',
  `relation_type` enum('apiToFunc','roleToDept','roleToPost') NOT NULL COMMENT '类型',
  `system_id` int(10) NOT NULL,
  PRIMARY KEY (`source_id`,`target_id`,`relation_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='绑定关系';


DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '用户组名称',
  `code` varchar(64) DEFAULT NULL COMMENT '编码',
  `role_type` enum('function','data') NOT NULL COMMENT '类型',
  `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户id',
  `dept_id` varchar(64) DEFAULT NULL COMMENT '关联部门id',
  `permissions` TEXT DEFAULT NULL COMMENT '权限',
  `remarks` varchar(100) DEFAULT NULL COMMENT '备注',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否系统默认',
  `is_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否对外显示',
  `tags` varchar(200) DEFAULT NULL COMMENT '标签',
  `system_id` int(10) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '可用状态(0:不可用;1:可用)',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '是否删除',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='角色';

DROP TABLE IF EXISTS `dict_category`;
CREATE TABLE `dict_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `code` varchar(32) DEFAULT NULL COMMENT '编号',
  `edit_scope` enum('gloal','system','tenant') NOT NULL COMMENT '编辑范围',
  `remarks` varchar(200) DEFAULT NULL COMMENT '备注',
  `internal` tinyint(1) DEFAULT 0 COMMENT '是否内置特殊类型',
  `system_id` int(10)  NOT NULL, 
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '激活状态（1：激活，0：未激活）',
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='字典分类';

DROP TABLE IF EXISTS `dict_item`;
CREATE TABLE `dict_item` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '名称',
  `code` varchar(32) DEFAULT NULL COMMENT '编号',
  `category_id` int(10) NOT NULL,
  `value` varchar(100) NOT NULL COMMENT '字典项值',
  `system_id` int(10)  NOT NULL, 
  `tenant_id` varchar(64) DEFAULT NULL COMMENT '租户id',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '激活状态（1：激活，0：未激活）',
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='字典项';


DROP TABLE IF EXISTS `sequence_rules`;
CREATE TABLE `sequence_rules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `code` varchar(32) DEFAULT NULL COMMENT '序列业务编号',
  `prefix` varchar(32) DEFAULT NULL COMMENT '前缀',
  `time_expr` varchar(32) DEFAULT NULL COMMENT '时间表达式',
  `seq_length` int(1) NOT NULL DEFAULT '6' COMMENT '自增序列位数',
  `first_sequence` int(10) NOT NULL DEFAULT '1' COMMENT '初始自增序列值',
  `last_sequence` int(10) NOT NULL DEFAULT '1' COMMENT '最新自增序列值',
  `random_type` ENUM('CHAR','NUMBER','ANY') DEFAULT 'any' COMMENT '随机字符串类型',
  `random_length` int(1) DEFAULT 0 COMMENT '随机字符串长度',
  `system_id` int(10)  NOT NULL, 
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '激活状态（1：激活，0：未激活）',
  `deleted` TINYINT(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(32) DEFAULT NULL COMMENT '更新人姓名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='序列生成规则';


DROP TABLE IF EXISTS `action_logs`;
CREATE TABLE `action_logs` (
  `id` varchar(32) NOT NULL,
  `log_type` varchar(16) NOT NULL,
  `system_id` int(10) DEFAULT NULL,
  `system_key` varchar(32)  DEFAULT NULL, 
  `module_key` varchar(32) DEFAULT NULL,
  `action_name` varchar(64) NOT NULL,
  `action_key` varchar(128) NOT NULL,
  `tenant_id` varchar(32)  DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `user_name` varchar(32) DEFAULT NULL,
  `client_type` varchar(32) DEFAULT NULL,
  `client_ip` varchar(20) DEFAULT NULL, 
  `action_at` datetime DEFAULT NULL,
  `input_data` TEXT DEFAULT NULL,
  `output_data` TEXT DEFAULT NULL,
  `biz_id` varchar(32) DEFAULT NULL,
  `trace_id` varchar(32) DEFAULT NULL,
  `successed` tinyint(1) DEFAULT NULL,
  `use_time` int(8) DEFAULT NULL,
  `exceptions` TEXT DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志';
