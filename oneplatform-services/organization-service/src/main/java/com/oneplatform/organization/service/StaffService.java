package com.oneplatform.organization.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mendmix.amqp.MQInstanceDelegate;
import com.mendmix.amqp.MQMessage;
import com.mendmix.common.guid.GUID;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.util.AssertUtil;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.mendmix.mybatis.plugin.pagination.PageExecutor.PageDataLoader;
import com.oneplatform.common.api.SequenceApi;
import com.oneplatform.common.constants.MqTopicNames;
import com.oneplatform.common.constants.SequeceCodes;
import com.oneplatform.common.dto.UserInfoParam;
import com.oneplatform.organization.dao.entity.DepartmentEntity;
import com.oneplatform.organization.dao.entity.PositionEntity;
import com.oneplatform.organization.dao.entity.StaffDepartmentEntity;
import com.oneplatform.organization.dao.entity.StaffEntity;
import com.oneplatform.organization.dao.mapper.DepartmentEntityMapper;
import com.oneplatform.organization.dao.mapper.PositionEntityMapper;
import com.oneplatform.organization.dao.mapper.StaffDepartmentEntityMapper;
import com.oneplatform.organization.dao.mapper.StaffEntityMapper;
import com.oneplatform.organization.dto.StaffDetails;
import com.oneplatform.organization.dto.param.StaffParam;
import com.oneplatform.organization.dto.param.StaffQueryParam;


/**
 * <br>
 * Class Name   : StaffService
 */
@Service
public class StaffService {

	@Autowired
    private StaffEntityMapper staffMapper;
	@Autowired
    private PositionEntityMapper positionMapper;
	@Autowired
	private DepartmentEntityMapper departmentMapper;
	@Autowired
    private StaffDepartmentEntityMapper staffDepartmentMapper;
    @Autowired
    private SequenceApi sequenceApi;

    @Transactional
    public String addStaff(StaffParam param) {
    	//
    	String staffNo = sequenceApi.getSequence(SequeceCodes.STAFF_NO);
    	String accountId = null;
    	StaffEntity entity = BeanUtils.copy(param, StaffEntity.class);
    	entity.setAccountId(accountId);
    	entity.setCode(staffNo);
        staffMapper.insertSelective(entity);
        //
        StaffDepartmentEntity relationEntity = null;
        if(StringUtils.isNotBlank(param.getPostionId())) {
        	PositionEntity positionEntity = positionMapper.selectByPrimaryKey(param.getPostionId());
        	AssertUtil.notNull(positionEntity, "岗位不存在");
        	//
        	relationEntity = new StaffDepartmentEntity();
        	relationEntity.setDepartmentId(positionEntity.getDepartmentId());
        	relationEntity.setPositionId(positionEntity.getId());
        	staffDepartmentMapper.insertSelective(relationEntity);
        }else if(StringUtils.isNotBlank(param.getDepartmentId())) {
        	DepartmentEntity departmentEntity = departmentMapper.selectByPrimaryKey(param.getDepartmentId());
        	AssertUtil.notNull(departmentEntity, "部门不存在");
        	relationEntity = new StaffDepartmentEntity();
        	relationEntity.setDepartmentId(param.getDepartmentId());
        }
        
        if(relationEntity != null) {
        	relationEntity.setStaffId(entity.getId());
        	relationEntity.setIsPrimary(true);
        	staffDepartmentMapper.insertSelective(relationEntity);
        }
        
        //创建账号
        if(param.isCreateAccount()) {
        	accountId = String.valueOf(GUID.guid());
        	UserInfoParam userInfoParam = BeanUtils.copy(param, UserInfoParam.class);
        	userInfoParam.setId(accountId);
        	userInfoParam.setName(staffNo);
        	userInfoParam.setRealname(param.getName());
        	userInfoParam.setPrincipalId(entity.getId());
        	MQInstanceDelegate.send(new MQMessage(MqTopicNames.CREATE_USER, userInfoParam));
        }
        
        return entity.getId();
    }

    public void updateStaff(StaffEntity entity) {
        staffMapper.updateByPrimaryKeySelective(entity);
    }

    public StaffEntity findById(String id) {
    	StaffEntity entity = staffMapper.selectByPrimaryKey(id);
    	return entity;
    }

    public Page<StaffDetails> pageQuery(PageParams pageParams, StaffQueryParam queryParam) {

        Page<StaffDetails> page = PageExecutor.pagination(pageParams, new PageDataLoader<StaffDetails>() {
            @Override
            public List<StaffDetails> load() {
                return staffMapper.findDetailsListByParam(queryParam);
            }
        });
        return page;
    }

}
