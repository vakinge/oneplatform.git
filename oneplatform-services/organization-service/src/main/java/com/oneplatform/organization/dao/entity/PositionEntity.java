package com.oneplatform.organization.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.oneplatform.common.dao.StandardBaseEntity;

@Table(name = "position")
public class PositionEntity extends StandardBaseEntity {
  
    /**
     * 岗位名称
     */
    private String name;
    
    private String code;
    
    private String type;
    
    /**
     * 部门ID
     */
    @Column(name = "department_id")
    private String departmentId;
    
    @Column(name = "is_leader")
    private Boolean isLeader;
    
    @Column(name = "superior_id")
    private String superiorId;
    
    @Transient
    private String departmentName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public Boolean getIsLeader() {
		return isLeader;
	}

	public void setIsLeader(Boolean isLeader) {
		this.isLeader = isLeader;
	}

	public String getSuperiorId() {
		return superiorId;
	}

	public void setSuperiorId(String superiorId) {
		this.superiorId = superiorId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	
    
}