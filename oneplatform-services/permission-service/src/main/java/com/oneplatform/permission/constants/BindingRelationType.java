package com.oneplatform.system.constants;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 
 * <br>
 * Class Name   : BindingRelationType
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年12月23日
 */
public enum BindingRelationType {
	apiToFunc,roleToDept,roleToPost;
	
	@JsonCreator
	public static BindingRelationType getEnum(String key){
		for(BindingRelationType item : values()){
			if(item.name().equals(key)){
				return item;
			}
		}
		return null;
	}
}
