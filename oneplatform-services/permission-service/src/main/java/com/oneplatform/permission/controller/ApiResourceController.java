package com.oneplatform.system.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.async.RetryAsyncTaskExecutor;
import com.mendmix.common.async.RetryTask;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.http.HttpRequestEntity;
import com.mendmix.common.model.ApiInfo;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.model.SelectOptGroup;
import com.mendmix.common.model.SelectOption;
import com.mendmix.common.util.LogMessageFormat;
import com.mendmix.springweb.model.AppMetadata;
import com.oneplatform.system.dao.entity.ApiResourceEntity;
import com.oneplatform.system.dao.entity.BizSystemModuleEntity;
import com.oneplatform.system.dto.ApiResource;
import com.oneplatform.system.dto.param.ApiResourceParam;
import com.oneplatform.system.dto.param.ApiResourceQueryParam;
import com.oneplatform.system.service.ApiResourceService;
import com.oneplatform.system.service.BizSystemService;

import io.swagger.annotations.ApiOperation;


@RestController("AppApiResourceController")
@RequestMapping("/apiResource")
public class ApiResourceController {

    private @Autowired ApiResourceService apiResourceService;
    private @Autowired BizSystemService systemService;

    
    @ApiOperation(value = "新增API")
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping(value = "add")
    @ResponseBody
    public IdParam<Integer> add(@RequestBody ApiResourceParam param) {
        return apiResourceService.addApiResource(param);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping(value = "delete")
    @ResponseBody
    public void delete(@RequestBody IdParam<Integer> param) {
        apiResourceService.deleteApiResource(param.getId());
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping("update")
    @ResponseBody
    public void update(@RequestBody ApiResourceParam param) {
        apiResourceService.updateApiResource(param);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping("toggle")
    @ResponseBody
    public void toggleApi(@RequestBody IdParam<Integer> param) {
        apiResourceService.switchApiResource(param.getId());
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @GetMapping(value = "{id}")
    @ResponseBody
    public ApiResource get(Integer id) {
        return apiResourceService.getApiResourceById(id);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping("list")
    @ResponseBody
    public Page<ApiResource> pageQry(@RequestBody PageQueryRequest<ApiResourceQueryParam> queryParam) {
        return apiResourceService.pageQry(new PageParams(queryParam.getPageNo(),queryParam.getPageSize()),queryParam.getExample());
    }

    
    @GetMapping("options")
    @ResponseBody
    public List<SelectOption> options(@RequestParam(name="moduleId",required=false) Integer moduleId) {
    	List<ApiResourceEntity> list = apiResourceService.findByModuleId(moduleId);
    	return list.stream().map(e -> {
            return new SelectOption(e.getId().toString(),e.getName());
        }).collect(Collectors.toList());
    }

    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/treeOptions")
    @ResponseBody
    public List<SelectOptGroup> apiResourceTree() {
    	
    	List<BizSystemModuleEntity> modules = systemService.getSystemModules();
    	List<SelectOptGroup> result = new ArrayList<>(modules.size());
    	SelectOptGroup optGroup;
    	Map<Integer, SelectOptGroup> tmpMapping = new HashMap<>(modules.size());
    	for (BizSystemModuleEntity module : modules) {
    		optGroup = new SelectOptGroup(module.getName(), null);
    		result.add(optGroup);
    		tmpMapping.put(module.getId(), optGroup);
		}
    	ApiResourceQueryParam queryParam = new ApiResourceQueryParam();
    	queryParam.setEnabled(true);
    	queryParam.setPermissionLevel(PermissionLevel.PermissionRequired.name());
    	List<ApiResource> apis = apiResourceService.listByQueryParam(queryParam);
    	
    	SelectOption option;
    	for (ApiResource apiResource : apis) {
			if(tmpMapping.containsKey(apiResource.getModuleId())) {
				option = new SelectOption(apiResource.getId().toString(),apiResource.getName());
				tmpMapping.get(apiResource.getModuleId()).getOptions().add(option);
			}
		}
    	//
		return result;
    }
    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
    @ResponseBody
    @PostMapping("syncModuleApis")
    public void syncModuleApis(@RequestParam(name = "moduleId",required = false) Integer moduleId) {
    	if(moduleId != null) {
    		BizSystemModuleEntity moduleEntity = systemService.findSystemModuleById(moduleId);
        	syncModuleApis(moduleEntity);
    	}else {
    		List<BizSystemModuleEntity> modules = systemService.getSystemModules();
    		for (BizSystemModuleEntity module : modules) {
    			RetryAsyncTaskExecutor.execute(new RetryTask() {
					@Override
					public String traceId() {
						return LogMessageFormat.buildLogHeader("syncModuleApis", module.getId());
					}
					
					@Override
					public boolean process() throws Exception {
						syncModuleApis(module);
						return true;
					}
				});
    			
			}
    	}
    	
    }


	private void syncModuleApis(BizSystemModuleEntity moduleEntity) {
		Integer moduleId = moduleEntity.getId();
		String url;
		if(moduleEntity.getProxyUrl() == null) {
			url = "http://"+ moduleEntity.getServiceId() + "/metadata";
		}else {
			url = moduleEntity.getProxyUrl() + "/metadata";
		}
    	if(url.startsWith("lb:")) {
    		url = url.replace("lb:", "http:");
    	}
    	AppMetadata metadata = HttpRequestEntity.post(url).internalCall().execute().toObject(AppMetadata.class);
    	if(metadata == null || metadata.getApis().isEmpty())return;
    	
    	Collection<ApiInfo> apis = metadata.getApis();
    	List<ApiResourceEntity> entities;
		entities = apis.stream().map(api -> {
			ApiResourceEntity entity = new ApiResourceEntity();
			entity.setName(api.getName());
			entity.setMethod(api.getMethod());
			entity.setUri(api.getUri());
			entity.setPermissionLevel(api.getPermissionLevel().toString());
			entity.setModuleId(moduleId);
			entity.setOpenapi(api.isOpenApi());
			return entity;
		}).collect(Collectors.toList());
		
		apiResourceService.batchUpdateApiResource(moduleId, entities);
	}

}
