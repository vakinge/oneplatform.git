package com.oneplatform.system.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oneplatform.system.constants.FunctionResourceType;
import com.oneplatform.system.constants.RoleType;
import com.oneplatform.system.dao.StandardBaseEntity;
import com.oneplatform.system.dao.entity.ApiResourceEntity;
import com.oneplatform.system.dao.entity.FunctionResourceEntity;
import com.oneplatform.system.dao.entity.UserRoleEntity;
import com.oneplatform.system.dao.mapper.ApiResourceEntityMapper;
import com.oneplatform.system.dao.mapper.FunctionResourceEntityMapper;
import com.oneplatform.system.dao.mapper.UserRoleEntityMapper;
import com.oneplatform.system.dto.param.ResourceScopeQueryParam;

/**
 * <br>
 * Class Name : UserPermissionService
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年1月2日
 */
@Service
public class UserPermissionService {

    @Autowired
    private UserRoleEntityMapper userGroupMapper;
    @Autowired
    private ApiResourceEntityMapper apiResourceMapper;
    @Autowired
    private FunctionResourceEntityMapper functionResourceMapper;

 
    public List<UserRoleEntity> findUserAssignRoles(RoleType type,String userId,String departmentId) {
        return userGroupMapper.findGrantedUserRoles(type.name(), userId, departmentId);
    }

    public List<String> findGrantedUserRoleIds(RoleType type,String userId,String departmentId){
    	List<UserRoleEntity> roles = findUserAssignRoles(type,userId,departmentId);
    	List<String> roleIds = roles.stream().map(o -> o.getId().toString()).collect(Collectors.toList());
    	return roleIds;
    }

    public List<ApiResourceEntity> findUserGrantedApis(ResourceScopeQueryParam param) {
    	//用户角色
    	List<String> roleIds = findGrantedUserRoleIds(RoleType.function,param.getUserId(),param.getDepartmentId());
        if(roleIds.isEmpty())return new ArrayList<>(0);
    	List<ApiResourceEntity> apiResources = apiResourceMapper.findByRolesGrantFunctions(roleIds);
        return apiResources;
    }

    
    public List<FunctionResourceEntity> findRoleGrantedResources(FunctionResourceType resourceType,String clientType,Integer  roleId) {
    	List<FunctionResourceEntity> entities = functionResourceMapper.findRoleGrantedResources(Arrays.asList(roleId.toString()), clientType, resourceType.name());
        //查询父级（由于一些情况数据库只保存了子级菜单，要显示树形结构还需要）
        recursionQueryParentResources(clientType,entities);
        return entities;
    }

    public List<FunctionResourceEntity> findUserGrantedResources(ResourceScopeQueryParam param) {

    	List<String> roleIds = findGrantedUserRoleIds(RoleType.function, param.getUserId(), param.getDepartmentId());
    	if(roleIds.isEmpty())return new ArrayList<>(0);
        List<FunctionResourceEntity> entities = functionResourceMapper.findRoleGrantedResources(roleIds, param.getClientType(), param.getType());
        //开放访问
        entities.addAll(functionResourceMapper.findOpenAcessResources(param.getClientType(), param.getType()));
        
        recursionQueryParentResources(param.getClientType(),entities);
        
        return entities;
    }

    /**
     * 递归查询父级菜单资源并合并结果
     *
     * @param entities
     */
    private void recursionQueryParentResources(String clientType,List<FunctionResourceEntity> entities) {

    	if(entities.isEmpty())return;
        Map<Integer, FunctionResourceEntity> parentMap = functionResourceMapper.findParentMenus(clientType)
                .stream()
                .collect(Collectors.toMap(FunctionResourceEntity::getId, o -> o));
        //如果数据表已经保存了父级菜单
        for (FunctionResourceEntity entity : entities) {
        	parentMap.remove(entity.getId());
        }
        if(parentMap.isEmpty())return;
        
        Map<Integer, FunctionResourceEntity> tmpParentMap = new HashMap<>();
        for (FunctionResourceEntity entity : entities) {
            recursionAllRelateParentMenus(parentMap, tmpParentMap, entity);
        }
        if (!tmpParentMap.isEmpty()) entities.addAll(tmpParentMap.values());
        entities.sort(Comparator.comparing(StandardBaseEntity::getId));
    }

    private void recursionAllRelateParentMenus(Map<Integer, FunctionResourceEntity> parentMap, Map<Integer, FunctionResourceEntity> result, FunctionResourceEntity entity) {
        if (entity.getParentId() == null || entity.getParentId() == 0) return;
        //当前已经是父级菜单
        if(parentMap.containsKey(entity.getId()))return;
        if (parentMap.containsKey(entity.getParentId())) {
            FunctionResourceEntity tmpParent = parentMap.get(entity.getParentId());
            if (result.containsKey(tmpParent.getId())) return;
            result.put(tmpParent.getId(), tmpParent);
            recursionAllRelateParentMenus(parentMap, result, tmpParent);
        }
    }


}
