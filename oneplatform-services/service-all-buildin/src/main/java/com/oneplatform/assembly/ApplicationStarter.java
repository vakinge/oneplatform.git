package com.oneplatform.assembly;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mendmix.springcloud.starter.BaseApplicationStarter;

/**
 * 
 * 
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">jiangwei</a>
 * @date May 22, 2022
 */
@SpringBootApplication(exclude = {QuartzAutoConfiguration.class})
@EnableDiscoveryClient
@EnableTransactionManagement
@ComponentScan(value = {
		"com.mendmix.springcloud.autoconfigure",
		"com.oneplatform.*.dao",
		"com.oneplatform.*.service",
		"com.oneplatform.*.event",
		"com.oneplatform.*.task",
		"com.oneplatform.*.controller",
		"com.oneplatform.assembly"
})
public class ApplicationStarter extends BaseApplicationStarter{

	public static void main(String[] args) {
		long starTime = before(args);
		new SpringApplicationBuilder(ApplicationStarter.class).web(WebApplicationType.SERVLET).run(args);
		after(starTime);
	}
	
}
