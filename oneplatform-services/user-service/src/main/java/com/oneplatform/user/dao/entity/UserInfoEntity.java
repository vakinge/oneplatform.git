package com.oneplatform.user.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.oneplatform.common.dao.StandardBaseEntity;

@Table(name = "user_info")
public class UserInfoEntity extends StandardBaseEntity {
   
    private String name;
    
    private String nickname;
    
    private String realname;

    private String mobile;

    private String password;

    private String avatar;

    @Column(name = "id_number")
    private String idNumber;

    private String gender;

    private Date birtyday;

    private String address;
    
    /**
     * 是否已认证
     */
    @Column(name = "is_certified")
    private Boolean isCertified;
    
    private Integer level;

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
   
    public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
     * @return mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * @return id_number
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * @param idNumber
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * @return gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return birtyday
     */
    public Date getBirtyday() {
        return birtyday;
    }

    /**
     * @param birtyday
     */
    public void setBirtyday(Date birtyday) {
        this.birtyday = birtyday;
    }
    

    public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
     * 获取是否已认证
     *
     * @return is_certified - 是否已认证
     */
    public Boolean getIsCertified() {
        return isCertified;
    }

    /**
     * 设置是否已认证
     *
     * @param isCertified 是否已认证
     */
    public void setIsCertified(Boolean isCertified) {
        this.isCertified = isCertified;
    }

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public static void main(String[] args) {
		System.out.println(BCrypt.hashpw("123456", BCrypt.gensalt()));
	}
 
}