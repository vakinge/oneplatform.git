package com.oneplatform.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.common.dto.UserInfoParam;
import com.oneplatform.user.dao.entity.UserInfoEntity;
import com.oneplatform.user.dto.UserInfo;
import com.oneplatform.user.dto.UserScope;
import com.oneplatform.user.dto.param.UserQueryParam;
import com.oneplatform.user.dto.param.UserValidateParam;
import com.oneplatform.user.service.UserInfoService;

/**
 * 
 * <br>
 * Class Name   : UserInfoController
 *
 */
@RestController
@RequestMapping("/")
public class UserInfoController {

	private @Autowired UserInfoService userService;

	@ApiMetadata(actionName = "查询用户基本信息",permissionLevel = PermissionLevel.LoginRequired)
	@GetMapping("baseInfo/{id}")
	public UserInfo getById(@PathVariable(value = "id") String id) {
		UserInfoEntity entity = userService.findById(id);
		return BeanUtils.copy(entity, UserInfo.class);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "add")
	public IdParam<String> add(@RequestBody @Validated UserInfoParam param) {
		String id = userService.addUser(param);
		return new IdParam<>(id);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "update")
	public void update(@RequestBody @Validated UserInfoParam param) {
		UserInfoEntity entity = BeanUtils.copy(param, UserInfoEntity.class);
		userService.updateUser(entity);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@PostMapping("list")
	public Page<UserInfo> pageQuery(@RequestBody PageQueryRequest<UserQueryParam> param) {
		if (param.getExample() == null)
			param.setExample(new UserQueryParam());
		Page<UserInfo> page = userService.pageQuery(new PageParams(param.getPageNo(), param.getPageSize()),
				param.getExample());
		return page;
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "toggle")
	public void toggle(@RequestBody IdParam<String> param) {
		UserInfoEntity entity = userService.findById(param.getId());
		entity.setEnabled(!entity.getEnabled());
		userService.updateUser(entity);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
	@PostMapping(value = "delete")
	public void delete(@RequestBody IdParam<String> param) {
		UserInfoEntity entity = userService.findById(param.getId());
		entity.setDeleted(true);
		userService.updateUser(entity);
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous, actionLog = true)
	@PostMapping(value = "validate")
	public UserInfo validate(@RequestBody @Validated UserValidateParam param) {
		return userService.validateUser(param.getAccount(), param.getPassword());
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous, actionLog = false)
	@GetMapping(value = "scopes")
	public List<UserScope> validate(@RequestParam("userId") String id) {
		return userService.findUserScopes(id);
	}
	
}
