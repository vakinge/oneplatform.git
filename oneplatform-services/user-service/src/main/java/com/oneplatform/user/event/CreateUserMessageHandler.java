/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.user.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.amqp.MQMessage;
import com.mendmix.amqp.MQTopicRef;
import com.mendmix.amqp.MessageHandler;
import com.oneplatform.common.constants.MqTopicNames;
import com.oneplatform.common.dto.UserInfoParam;
import com.oneplatform.user.service.UserInfoService;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Jul 31, 2022
 */
@Service
@MQTopicRef(MqTopicNames.CREATE_USER)
public class CreateUserMessageHandler implements MessageHandler {

	private @Autowired UserInfoService userService;
	
	@Override
	public void process(MQMessage message) throws Exception {
		UserInfoParam param = message.toObject(UserInfoParam.class);
		userService.addUser(param);
	}

}
