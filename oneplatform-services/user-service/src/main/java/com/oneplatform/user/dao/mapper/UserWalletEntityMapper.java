package com.oneplatform.user.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.user.dao.entity.UserWalletEntity;

public interface UserWalletEntityMapper extends BaseMapper<UserWalletEntity, String> {
}